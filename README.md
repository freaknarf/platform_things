# README #

This README would normally document whatever steps are necessary to get your application up and running.

So far : 

=================
==Control stuff==
=================

controlBaddies.gml : raw start for controlling AI and mechanisms
controlPause.gml : raw menu with options and pause
controlPlayer.gml : player actions handled by controller : see below.

=================
==Player  moves==
=================

playerBump.gml : bump on baddies
playerClimb.gml : climb on ladders
playerClimbMoves.gml : climbing moves and rules
playerCollide.gml : players's collisions
playerDeath.gml : player's death
playerDie.gml : player's death (anim)
playerJump.gml : player can jump : 3 state levels + double jump
playerLand.gml : player "perfect" collision
playerMoveAndCollide.gml : global moves script
playerMovesRegular.gml : normal moves and speed settings
playerPick.gml : coin picking
playerRules.gml : out of screen rules etc
playerSetMoves.gml : I/O 

17/04/01

Baddies :
	Walkers : 2 ai styles : thinkers and regular patrollers
Player : 
	crounch

Swimming issue fixed  (jump too high)
Ladders issue fixed (while loop crash + jump too high + height adjustment)
Bump issue fixed (not bumping when grounded :p)

Some code refactor and new scripts.

///

### What is this repository for? ###

This is another test project, trying to gather some platformers elements, with GMS 1.99.
There is a player workaround so far with some moves and actions.
Keys : Arrows + space + escape as usual.

### How do I get set up? ###

Open this in GMS 1.99

### Contribution guidelines ###

### Who do I talk to? ###

Knarf