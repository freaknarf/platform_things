//Pause state control
if start{
    pause=!pause
}
//Instance active control during pause. Display / control of the menu

if (pause){

    instance_deactivate_all(true);  
    if !instance_exists(oInput)
        instance_create(0,0,oInput);
    instance_activate_object(oInput);
    
    if !instance_exists(oMenu)
        instance_create(0,0,oMenu);
    instance_activate_object(oMenu);
    
        oMenu.selected = oMenu.selected - up + down
        oMenu.selected = clamp(oMenu.selected,0,array_length_1d(oMenu.option)-1)
               
    
}
else{
    if instance_exists(oMenu)
        with(oMenu) 
            instance_destroy();
    instance_activate_all();
}

