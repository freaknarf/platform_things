//Player Set Moves

    if B spdx=3 else spdx=2


    if !feedback
    movx=spdx*(-left+right)*(!climbing)
    else
    movx=spdx*(dirFB)*(!climbing)
    
    movy=spdy*((grav-jumping)*(!climbing)+climbing*(-up+down))
    
    

    movx=clamp(movx,-sprite_width,sprite_width)
    movy=clamp(movy,-sprite_height,sprite_height)
