if instance_place(x,y,oCoin){
    nCoins+=1
    coin=instance_place(x,y,oCoin)
    with coin {
    instance_destroy();
    }
    show_debug_message("nCoins : "+string(nCoins))
    }
    
    
if instance_place(x,y,oBonus){
    bonus=instance_place(x,y,oBonus)
    show_debug_message(string(bonus.bonusType))
    ds_map_replace(bonusMap,string(bonus.bonusType),bonus.bonusValue)
    with bonus {
    instance_destroy();
    }
    
    }
